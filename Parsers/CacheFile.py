import tempfile
import os.path
import numpy as np
import cPickle

class CacheFile(object):
    '''
    Class handles the data fetching

    Caches the data if it's been fetched 
    before
    '''
    basedir = ""
    delimiter = None

    def __init__(self, filename, clear, verbose):
        super(CacheFile, self).__init__()
        self.filename = filename
        self.clear = clear
        self.verbose = verbose

    def sourceFilename(self):
        return os.path.join(self.basedir, self.filename)
    
    def cachedDataFilename(self):
        return os.path.join(tempfile.gettempdir(),
                os.path.splitext(self.filename)[0] + ".cpickle")

    def _getFromAscii(self):
        return np.loadtxt(self.sourceFilename(), unpack=True,
                        delimiter=self.delimiter)

    def _getFromCache(self):
        return cPickle.load(open(self.cachedDataFilename(), 'r'))


    def getData(self):
        if not os.path.isfile(self.cachedDataFilename()):
            # Must fetch the data and cpickle the file
            if self.verbose: print "Caching file"
            x, y = self._getFromAscii()
            cPickle.dump([x, y], open(self.cachedDataFilename(), 'w'), protocol=2)
            return x, y
        else:
            # Cached file already exists, so return this data
            if self.verbose: print "File already cached"
            if self.clear:
                if self.verbose: print "*** Clearing cache"
                os.remove(self.cachedDataFilename())
                data = self._getFromAscii()
                cPickle.dump(data, open(self.cachedDataFilename(), 'w'), protocol=2)
                return data
            else:
                return cPickle.load(open(self.cachedDataFilename(), 'r'))


