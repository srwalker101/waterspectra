import unittest2
from CacheFile import CacheFile

class Extinction(CacheFile):
    basedir = "Transmission"
    def __init__(self, clear=False, verbose=False):
        super(Extinction, self).__init__("extinction.dat", clear, verbose)

    def getData(self):
        return super(Extinction, self).getData()



class TestExtinction(unittest2.TestCase):
    def setUp(self):
        self.e = Extinction()

    def test_setup(self):
        self.assertEqual(self.e.filename,
                "extinction.dat")

    def test_source_filename(self):
        self.assertEqual(self.e.sourceFilename(),
                "Transmission/extinction.dat")

    def test_get_data(self):
        data = self.e.getData()
        self.assertGreater(len(data), 0)



if __name__ == '__main__':
    unittest2.main()

