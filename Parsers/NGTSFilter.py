# -*- coding: utf-8 -*-

import numpy as np
import os.path
import tempfile
import cPickle
from CacheFile import CacheFile

class NGTSFilter(CacheFile):
    def __init__(self, extended, clear=False, verbose=False):
        if extended:
            super(NGTSFilter, self).__init__("NGTSFilter_custom.csv", clear, verbose)
        else:
            super(NGTSFilter, self).__init__("NGTSFilter.csv", clear, verbose)

        self.basedir = "Transmission"

if __name__ == '__main__':
    ng = NGTSFilter()
    x, y = ng.getData()
