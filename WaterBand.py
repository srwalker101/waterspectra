#!/usr/bin/env python

import sys
#import os
#import os.path
import argparse
#from subprocess import Popen, call, PIPE, STDOUT
import matplotlib.pyplot as plt
import numpy as np
#import srw
#import pyfits
import scipy.interpolate
import scipy.integrate
from jg import spectra
#from ppgplot import *
from Parsers.Transmission import Transmission
from Parsers.NGTSFilter import NGTSFilter




class App(object):
    '''
    Main application object for the project
    '''
    alpha = 0.2
    def __init__(self, args):
        '''
        Constructor
        '''
        super(App, self).__init__()
        self.args = args
        if self.args.airmass < 1.0: 
            raise RuntimeError("Airmass must be > 1.0")

        print "Airmass: %f" % self.args.airmass

        if self.args.disableextinction:
            print "Extinction disabled"
        else:
            print "Extinction enabled"

        if self.args.ngtsfilter:
            print "Using custom red sensitive filter"

        basictypes = ["F5V", "G5V", "K5V", "M5V"]
        #self.types = ["M%dV" % t for t in xrange(9)]
        #self.types = ["M5V",]
        self.types = self.args.spectype
        if not self.types:
            print "*** No spectral types specified, using the defaults"
            self.types = basictypes

        self.types = [t.upper() for t in self.types]


        self.transmission = Transmission(self.args.clearcache)
        Diameter = 20.
        Radius = Diameter / 2.
        self.area = np.pi * Radius**2
        self.labels = [[], []]
        self.run()

    def boxFunction(self):
        return np.array([[9000, 0], [9001, 1],
            [9999, 1], [10000, 0]])

    def run(self):
        '''
        Main function
        '''

        self.getTransmission()
        if self.args.plot: self.setUpPlot()
        self.getSpectra()
        if self.args.plot: self.finishPlot()


    def getTransmission(self):
        self.trWavlength, self.trTrans = self.transmission.getData(self.args.wc, self.args.airmass,
                disable=self.args.disableextinction)
        print "Water column depth: %dmm\n" % self.args.wc



        # Convert wav to angstroms
        self.trWavlength /= 1E-4
        # plt.plot(self.trWavlength, self.trTrans, 'r-')

        # Only include the range
        ind = self.trWavlength < 10000.
        self.trWavlength = self.trWavlength[ind]
        self.trTrans = self.trTrans[ind]



    def getFilter(self):
        ng = NGTSFilter(self.args.ngtsfilter, self.args.clearcache)
        x, y = ng.getData()


        # Get x in angstroms
        x *= 10.

        # Get y in fraction
        y /= 100.


        if self.args.plot:
            self.ngfiltAx = self.mainAx.twinx()
            if self.args.ngtsfilter:
                self.ngfiltAx.set_ylabel("Custom red NGTS Filter transmission")
                line = self.ngfiltAx.plot(x, y, 'k-', label="Custom Filter")
            else:
                self.ngfiltAx.set_ylabel("Proposed NGTS Filter transmission")
                line = self.ngfiltAx.plot(x, y, 'k-', label="NGTS Filter")

            self.ngfiltAx.set_ylim(0, 1.1)

        return np.array([a for a in zip(x, y)])

    def setUpPlot(self):
        self.fig = plt.figure(figsize=(11, 8))


    def getSpectra(self):
        if self.args.plot: self.mainAx = self.fig.add_subplot(111)
        for t in self.types:
            try:
                spec = spectra.spectra_pickles(t)
                # spec.quick_plot()
            except spectra.spectraError:
                '''
                Spectral type does not contain any data
                '''
                print "Cannot find spectral type %s" % t
            else:
                print "Spectral type: %s" % t

                #if self.args.normalise: spec.normalise()
                
                wavelength = spec.spc[:, 0]
                flux = spec.spc[:, 1]

                NGTSFilter = self.getFilter()
                spectraMag, center = spec.custom_mag(self.getFilter())


                print "\tStar in passband has magnitude of %f" % spectraMag

                # Only select the wavelengths between 9000 and 10000 A
                ind = wavelength <= 10000
                wavelength = wavelength[ind]
                flux = flux[ind]
                    
                # # Interpolate the model spectra on the transmission scale
                self.model = scipy.interpolate.interp1d(wavelength, flux, kind="linear")

                # # Now get the interpolated model flux
                interpModel = self.model(self.trWavlength)

                # # Get the observed spectrum
                observedSpectrum = self.trTrans * interpModel
                if self.args.plot: self.mainAx.plot(self.trWavlength, observedSpectrum, 'r-', alpha=self.alpha, zorder=-10)


                # Create spectra object
                obSpectrumSpec = spectra.spectra(l=self.trWavlength, f=observedSpectrum, lunit='A', 
                        funit='fla_cgs')
                observedMag, cfreq = obSpectrumSpec.custom_mag(self.getFilter())
                print "\tStar has observed magnitude of %f" % observedMag

                diff = observedMag - spectraMag
                print "\tDifference: %f magnitudes\n" % diff


                if self.args.plot: line = self.mainAx.plot(wavelength, flux, label=r"%s, $\Delta$: %f mag" % (t, diff),
                             marker=None)



                
    def finishPlot(self):
        self.mainAx.set_ylabel(r"F$_\lambda$ / ergs cm$^{-2}$ s$^{-1}$ A$^{-1}$")
        self.mainAx.set_xlabel("Wavelength / $\AA$")
        #plt.xlim(3000, 10000)
        plt.title("%dmm water column depth" % self.args.wc)
        # plt.legend(self.labels[0], self.labels[1], 'best')

        ylim = self.mainAx.get_ylim()
        self.mainAx.plot([0, 1], [-100, -100], 'r-', label="Observed spectrum", 
                alpha=self.alpha)
        self.mainAx.set_ylim(*ylim)
        handles, labels = self.mainAx.get_legend_handles_labels()

        try:
            handles2, labels2 = self.ngfiltAx.get_legend_handles_labels()

            handles.extend(handles2)
            labels.extend(labels2)
        except AttributeError:
            pass

        plt.legend(handles, labels, loc='best')
        self.mainAx.set_xlim(2000, 10000)

        plt.show()





if __name__ == '__main__':
    try:
        epilog = '''
        Command line options allow any combination of: spectral type,
        filter type, airmass and extinction handling with optional plotting
        '''
        description='''
        How will the varying water column over Paranal affect the NGTS instrument?
        With this code you can simulate different stellar types and how they 
        have their brightness altered due to the water column.
        '''
        parser = argparse.ArgumentParser(epilog=epilog, description=description)
        parser.add_argument("wc", type=int, choices=[10, 16, 30, 50])
        parser.add_argument("-s", "--spectype", help="Add spectral type to plot",
                action="append")
        #parser.add_argument("-n", "--normalise", action="store_true", 
                #default=False, help="Normalise spectra")
        parser.add_argument("-n", "--ngtsfilter", action='store_true',
                default=False, help="Plot custom NGTS filter")
        parser.add_argument("-d", "--disableextinction", action="store_true",
                default=False, help="Disable extinction")
        parser.add_argument("-c", "--clearcache", action="store_true",
                default=False, help="Clear all caches")
        parser.add_argument("-a", "--airmass", default=1.0, type=float,
                help="Airmass value, default 1.0")
        parser.add_argument("-v", "--verbose", action="store_true", 
                default=False, help="Verbose mode")
        parser.add_argument("-p", "--plot", help="Show plot",
                default=False, action="store_true")
        args = parser.parse_args()
        app = App(args)
    except KeyboardInterrupt:
        print >> sys.stderr, "Interrupt caught, exiting..."
        sys.exit(0)
